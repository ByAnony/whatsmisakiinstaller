SIRI="░██████╗██╗██████╗░██╗"
SIRI+="\n ██╔════╝  ║██╔══██╗  ║"
SIRI+="\n╚█████╗░██║██████╔╝██║"
SIRI+="\n░╚═══██╗██║██╔══██╗██║"
SIRI+="\n██████╔╝██║██║░░██║██║"
SIRI+="\n╚═════╝░╚═╝╚═╝░░╚═╝╚═╝"
SIRI+="\n🌀 WhatsSiri 🌀 INSTALLER"
MESAJ="\n🌀 WITH ANDROID"
MESAJ+="\n "
MESAJ+="\n📣 TELEGRAM: @Whatssiri"
MESAJ+="\n "
MESAJ+="\n "
MESAJ+="\n🌀 GEREKSİNİMLER KURULANA DEK TERMUX'TAN AYRILMAYIN!"
MESAJ+="\n "
MESAJ+="\n🌀 DO NOT LEAVE FROM TERMUX UNTIL THE REQUIREMENTS ARE INSTALLING!"
MESAJ+="\n "
YARDIM="\n📌 KURULUM %50, %70 VE %75'TE DURAKLADIĞINDA Y YAZIP ENTERLAYIN!"
YARDIM+="\n "
YARDIM+="\n📌 WRITE Y AND ENTER WHEN THE INSTALLATION IS PAUSED AT 50%, 70% AND 75%!"
YARDIM+="\n "
BOSLUK="\n "
echo -e $LOGO
echo -e $YARDIM
sleep 3
echo "⏳ PAKETLERİ GÜNCELLİYORUM..."
echo "⏳ UPDATING PACKAGES..."
echo -e $BOSLUK
apt update
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ NODE KURUYORUM..."
echo "⌛ INSTALLING NODE..."
echo -e $BOSLUK
apt install nodejs --fix-missing
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ GIT KURUYORUM..."
echo "⌛ INSTALLING GIT..."
echo -e $BOSLUK
pkg install git -y
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ 🌀 MİSAKİ STRİNG ALICI İNDİRİYORUM..."
echo "⌛ 🌀 MİSAKİ STRİNG INSTALLER..."
echo -e $BOSLUK
git clone https://github.com/ByMisakiMey/erzurumludeniyorbiseyler
clear
echo -e $BOSLUK
cd erzurumludeniyorbiseyler
echo -e $LOGO
echo -e $MESAJ
echo "⌛ BAİLEYS KURUYORUM..."
echo "⌛ INSTALLING BAİLEYS..."
echo -e $BOSLUK
npm install @adiwajshing/baileys
npm install chalk
node qr.js

